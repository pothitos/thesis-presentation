# Constraint Programming: Algorithms and Systems

__Nikolaos Pothitos__

Supervised by Prof. __Panagiotis Stamatopoulos__

The presentation of my [Ph.D.
thesis](http://cgi.di.uoa.gr/~pothitos/thesis.pdf) is
available
[here](http://cgi.di.uoa.gr/~pothitos/presentation.pdf).
